import React from "react";
import "./App.css";

const Text = ({ cor, nome }) => {
  return <h1 style={{ color: cor }}>Hello, {nome}</h1>;
};

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Text cor="pink" nome="Cláudio" />
      </header>
    </div>
  );
}

export default App;
